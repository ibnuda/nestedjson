{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}
module Main where

import           Protolude

import           Data.Aeson
import           Data.Aeson.Encode.Pretty
import           Data.Aeson.TH
import qualified Data.ByteString.Lazy     as BL
import           Data.String
import qualified Data.Text                as T
import qualified Data.Text.Encoding       as TE
import           Network.HTTP.Conduit     (simpleHttp)
import qualified Turtle                   as Turt

data Client = Client
  { _clientClientId      :: Text
  , _clientHostname      :: Text
  , _clientVersion       :: Text
  , _clientRemoteAddress :: Text
  , _clientState         :: Int
  , _clientReadyCount    :: Int
  , _clientInFlightCount :: Int
  , _clientMessageCount  :: Int -- Could be Int64
  }

client :: Client
client = Client "clientid" "clienthostname" "clientversion" "clientremoteaddress" 0 1 2 3


data Channel = Channel
  { channelChannelName  :: Text
  , channelDepth        :: Int
  , channelBackendDepth :: Int
  , channelMessageCount :: Int
  , channelClients      :: [Client]
  , channelPaused       :: Bool
  }

channel :: Channel
channel = Channel "channelname" 1 2 3 [client] False

data Topic = Topic
  { topicTopicName :: Text
  , topicChannels  :: [Channel]
  , topicDepth     :: Int
  }

topic :: Topic
topic = Topic "topicname" [channel] 1

data Something = Something
  { somethingVersion :: Text
  , somethingHealth  :: Text
  , somethingTopics  :: [Topic]
  }

something :: Something
something = Something "somethingversion" "somethinghealth" [topic]

deriveJSON defaultOptions ''Client
deriveJSON defaultOptions ''Channel
deriveJSON defaultOptions ''Topic

-- deriveJSON defaultOptions ''Something
instance ToJSON Something where
  toJSON (Something a b c) = object ["version" .= a, "health" .= b, "topics" .= c]
instance FromJSON Something where
  parseJSON = withObject "Something" $ \o ->
    Something
    <$> o .: "version"
    <*> o .: "health"
    <*> o .: "topic"

emulateLookupNSQStat :: Text -> IO Text
emulateLookupNSQStat thing = pure thing

createClient cid = Client cid "clienthostname" "clientversion" "clientremoteaddress" 0 1 2 3

main :: IO ()
main = do
  putLByteString $ encodePretty something

nsqStatisticURL :: String
nsqStatisticURL = "http://127.0.0.1:4151/stats"

statisticURL :: String -> String -> String
statisticURL topic channel = nsqStatisticURL ++ "?format=json&topic="++ topic ++ "&channel=" ++ channel

lookupNsqStatistic :: String -> IO BL.ByteString
lookupNsqStatistic statisticURL =  simpleHttp statisticURL

--- Loaded GHCi configuration from /tmp/ghci25902/ghci-script
--- *Main> :set -XOverloadedStrings
--- *Main> import Turtle
--- *Main Turtle> import Data.Text
--- *Main Turtle Data.Text> let kuda = "kuda" :: Data.Text.Text
--- *Main Turtle Data.Text> let aduk = "kuda" :: Turtle.Text
--- *Main Turtle Data.Text> aduk == kuda
--- True
--- *Main Turtle Data.Text>

processTopic :: T.Text ->  IO()
processTopic topic  = do
  jsonString <- lookupNsqStatistic $ statisticURL (T.unpack topic) "job"
  let statistic = eitherDecode jsonString :: Either String Something
  case statistic of
    Left err -> putStrLn err
    Right ps -> putLByteString $ encodePretty ps
